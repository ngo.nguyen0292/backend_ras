﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ras.API.Models
{
    public class Result<T>
    {
        public string Code { get; set; }

        public string Message { get; set; }

        public T Value { get; set; }

        public bool IsSuccess { get; set; }
    }
}