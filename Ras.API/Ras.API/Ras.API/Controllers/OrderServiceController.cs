﻿using Ras.API.Models;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json;
using System.Data;
using System;

namespace Ras.API.Controllers
{
    public class OrderServiceController : ApiController
    {
        public class FormData
        {
            public bool IsNotch { get; set; }
            public int PageSize { get; set; }
            public int Page { get; set; }
        }

        public class FormDataDetail
        {
            public int OrderId { get; set; }
        }

        [HttpGet]
        public DataTable GetOrderDaily(FormData form)
        {        
            DataSet data = new DataSet();
            using (ExecuteDatabase connFunc = new ExecuteDatabase())
            {
                data = connFunc.ExecuteDataSet("sp_GetOrderList",
                    CommandType.StoredProcedure,
                    "@IsNotch", SqlDbType.Bit, form.IsNotch,
                    "@Page", SqlDbType.Int, form.Page,
                    "@PageSize", SqlDbType.Int, form.PageSize);
            }

            data.Tables[0].Columns.Add("Detail", typeof(Object));
            foreach (DataRow row in data.Tables[0].Rows)
            {
                string query = string.Format("OrderId = '{0}'", row["OrderId"]);
                row["Detail"] = data.Tables[1].Select(query).Length > 0 ? data.Tables[1].Select(query).CopyToDataTable() : null;
            }

            return data.Tables[0];
        }


        public DataTable GetOrderDetail(int OrderId)
        {
            DataSet data = new DataSet();
            using (ExecuteDatabase connFunc = new ExecuteDatabase())
            {
                data = connFunc.ExecuteDataSet("sp_GetOrderById",
                    CommandType.StoredProcedure,
                    "@OrderId", SqlDbType.Int, OrderId);
            }

            return data.Tables[0];
        }
    }
}
