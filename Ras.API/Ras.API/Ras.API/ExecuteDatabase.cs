﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Ras.API
{
    public class ExecuteDatabase : IDisposable
    {
        static private SqlConnection _conn = null;
        public SqlConnection Conn
        {
            get { return _conn; }
            set { _conn = value; }
        }

        public void Dispose()
        {
            //Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ExecuteDatabase()
        {
            //Dispose(false);
        }

        public ExecuteDatabase()
        {
            CreateConnect();
        }

        private void CreateConnect()
        {

            string connectionString = string.Format(@"Server={0}; Initial Catalog={1}; User ID={2};Password={3};Trusted_Connection=false; ", "118.69.183.41,1433", "test", "testadmin", "EOUg88CDo9aJznr");
            _conn = new SqlConnection(connectionString);
            if (_conn.State == ConnectionState.Closed) _conn.Open();

        }

        public string ExecuteDatabaseLog(string s)
        {
            if (_conn.State == ConnectionState.Closed) _conn.Open();
            SqlCommand command = _conn.CreateCommand();
            SqlTransaction transaction = _conn.BeginTransaction();
            command.Transaction = transaction;
            command.CommandText = @s;
            try
            {
                command.ExecuteNonQuery();
                transaction.Commit();
                EnterLog(s);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                transaction.Dispose();
                if (_conn != null) _conn.Dispose();
            }
        }

        public string ExecuteDatabaseNoLog(string s)
        {
            if (_conn.State == ConnectionState.Closed) _conn.Open();
            SqlCommand command = _conn.CreateCommand();
            SqlTransaction transaction = _conn.BeginTransaction();
            command.Transaction = transaction;
            command.CommandText = @s;
            try
            {
                command.ExecuteNonQuery();
                transaction.Commit();

                if (_conn.State == ConnectionState.Open) _conn.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                transaction.Dispose();
                if (_conn != null) _conn.Dispose();
            }
        }

        public DataTable LoadDataSource_Table(string s)
        {
            try
            {
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                SqlCommand cmd = new SqlCommand(@s, _conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable table = new DataTable("myTable");
                da.Fill(table);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return table;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (_conn != null) _conn.Dispose();
            }
        }

        public DataSet LoadDataSource_DataSet(string s)
        {
            try
            {
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                SqlCommand cmd = new SqlCommand(@s, _conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return ds;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (_conn != null) _conn.Dispose();
            }
        }

        public DataTable LoadPara(string paraTypeName)
        {
            try
            {
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                string s = string.Format(@"SELECT P.ID,P.Name,P.Value FROM SPA_Parameter AS P 
                INNER JOIN  SPA_Parameter_Type AS T ON P.ParaType_ID=T.ID
                where T.Name = N'{0}' AND P.State = 1", paraTypeName.Trim());
                SqlCommand cmd = new SqlCommand(@s, _conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable table = new DataTable("myTable");
                da.Fill(table);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return table;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (_conn != null) _conn.Dispose();
            }
        }

        public int ExecuteScalar(string s)
        {
            int ID;
            if (_conn.State == ConnectionState.Closed) _conn.Open();
            if (!s.EndsWith(";"))
            {
                s = s + ";";
            }
            SqlCommand command = _conn.CreateCommand();
            SqlTransaction transaction = _conn.BeginTransaction();
            command.Transaction = transaction;
            command.CommandText = @s + "SELECT CAST(scope_identity() AS int);";
            try
            {
                ID = (int)command.ExecuteScalar();
                transaction.Commit();
                EnterLog(s);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return ID;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
                if (_conn != null) _conn.Dispose();
            }
        }

        public void EnterLog(string sqlString, string filename = "", string functioname = "")
        {
            //try
            //{
            //    if (_conn.State == ConnectionState.Closed) _conn.Open();
            //    string sql = "INSERT INTO KIM_Log(Content,FileName,FunctionName,Created,Created_By,State) VALUES(@param1,@param2,@param3,@param4,@param5,@param6)";
            //    string currentPrefix = ConfigurationManager.AppSettings["prefixTableNameCurrent"];
            //    string newPrefix = ConfigurationManager.AppSettings["prefixTableNameNew"];
            //    if (currentPrefix != newPrefix)
            //        sql = sql.Replace(currentPrefix, newPrefix);
            //    SqlCommand cmd = new SqlCommand(sql, _conn);
            //    cmd.Parameters.AddWithValue("@param1", sqlString);
            //    cmd.Parameters.AddWithValue("@param2", filename);
            //    cmd.Parameters.AddWithValue("@param3", functioname);
            //    cmd.Parameters.AddWithValue("@param4", new Common().GetDateTimeNow().ToString("yyyy-MM-dd HH:mm:ss"));
            //    cmd.Parameters.AddWithValue("@param5", Global.sys_userID);
            //    cmd.Parameters.AddWithValue("@param6", 1);
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}
        }

        public DataTable ExecuteDataTable(string sql, CommandType commandType, params object[] pars)
        {
            try
            {
                if (_conn.State == ConnectionState.Closed) _conn.Open();
                SqlCommand com = new SqlCommand(sql, _conn);
                com.CommandType = commandType;
                com.CommandTimeout = 10000;
                for (int i = 0; i < pars.Length; i += 3)
                {
                    SqlParameter par = new SqlParameter(pars[i].ToString(), pars[i + 1]);
                    par.Value = pars[i + 2];
                    com.Parameters.Add(par);
                }

                SqlDataAdapter dad = new SqlDataAdapter(com);
                //EnterLog(sql);
                DataTable dt = new DataTable();
                dad.Fill(dt);
                if (_conn.State == ConnectionState.Open) _conn.Close();
                return dt;
            }

            finally
            {
                if (_conn != null) _conn.Dispose();
            }
        }

        /// <summary>
        /// Execute a SQL and return a Datatable and keep Connection(not close)
        /// </summary>
        /// <param name="con">SqlConnection</param>
        /// <param name="sql">sql string</param>
        /// <param name="commandType">CommandType</param>
        /// <param name="pars">Sql parameter: "@Name", SqlDbType, Value ("@id",SqlDbType.int, 1 [, ...])</param>
        /// <returns>DataTable</returns>
        public DataTable ExecuteDataTableLog(string sql,
            CommandType commandType,
            params object[] pars)
        {

            if (_conn.State == ConnectionState.Closed) _conn.Open();
            SqlCommand com = new SqlCommand(sql, _conn);
            com.CommandType = commandType;
            com.CommandTimeout = 10000;
            for (int i = 0; i < pars.Length; i += 3)
            {
                SqlParameter par = new SqlParameter(pars[i].ToString(), pars[i + 1]);
                par.Value = pars[i + 2];
                com.Parameters.Add(par);
            }

            SqlDataAdapter dad = new SqlDataAdapter(com);
            EnterLog(sql);
            DataTable dt = new DataTable();
            dad.Fill(dt);
            if (_conn.State == ConnectionState.Open) _conn.Close();
            return dt;
        }


        /// <summary>
        /// Execute a SQL and return a Datatable and keep Connection(not close)
        /// </summary>
        /// <param name="con">SqlConnection</param>
        /// <param name="sql">sql string</param>
        /// <param name="commandType">CommandType</param>
        /// <param name="pars">Sql parameter: "@Name", SqlDbType, Value ("@id",SqlDbType.int, 1 [, ...])</param>
        /// <returns>DataTable</returns>
        public DataSet ExecuteDataSet(string sql, CommandType commandType, params object[] pars)
        {
            if (_conn.State == ConnectionState.Closed) _conn.Open();
            SqlCommand com = new SqlCommand(sql, _conn);
            com.CommandType = commandType;
            com.CommandTimeout = 10000;
            for (int i = 0; i < pars.Length; i += 3)
            {
                SqlParameter par = new SqlParameter(pars[i].ToString(), pars[i + 1]);
                par.Value = pars[i + 2];
                com.Parameters.Add(par);
            }

            SqlDataAdapter dad = new SqlDataAdapter(com);
            //EnterLog(sql);
            DataSet ds = new DataSet();
            dad.Fill(ds);
            if (_conn.State == ConnectionState.Open) _conn.Close();
            return ds;
        }

        public DataSet ExecuteDataSetLog(string sql,
            CommandType commandType,
            params object[] pars)
        {
            if (_conn.State == ConnectionState.Closed) _conn.Open();
            SqlCommand com = new SqlCommand(sql, _conn);
            com.CommandType = commandType;
            com.CommandTimeout = 10000;
            for (int i = 0; i < pars.Length; i += 3)
            {
                SqlParameter par = new SqlParameter(pars[i].ToString(), pars[i + 1]);
                par.Value = pars[i + 2];
                com.Parameters.Add(par);
            }

            SqlDataAdapter dad = new SqlDataAdapter(com);
            EnterLog(sql);
            DataSet ds = new DataSet();
            dad.Fill(ds);
            if (_conn.State == ConnectionState.Open) _conn.Close();
            return ds;
        }
    }
}